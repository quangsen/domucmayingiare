<?php
// HTTP
define('HTTP_SERVER', 'http://domucmayingiare.dev/');
define('HTTP_IMAGE', 'http://domucmayingiare.dev/image/');

// HTTPS
define('HTTPS_SERVER', '');
define('HTTPS_IMAGE', '');

// DIR

define('DIR', 'D:\xampp\htdocs\domucmayingiare\\');

define('DIR_APPLICATION', DIR . 'catalog\\');
define('DIR_SYSTEM', DIR.'system\\');
define('DIR_DATABASE', DIR_SYSTEM . 'database\\');
define('DIR_LANGUAGE', DIR . 'catalog\language\\');
define('DIR_TEMPLATE', DIR . 'catalog/view/theme/');
define('DIR_CONFIG', DIR_SYSTEM . 'config\\');
define('DIR_IMAGE', DIR . 'image\\');
define('DIR_CACHE', DIR_SYSTEM . 'cache\\');
define('DIR_DOWNLOAD', DIR . 'download\\');
define('DIR_LOGS', DIR_SYSTEM . 'logs\\');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'domucmayingiare');
define('DB_PREFIX', '');
?>